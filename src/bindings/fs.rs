use crate::*;

/// Registers a new file handler.
///
/// File handlers are the backend of the `fs` system.
/// They implement typical filesystem operations, like opening and writing to files or mounting paths.
/// They also register a set of file types, that they can operate on.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::fs::{};
/// # let file_handler: *const emf_file_handler_interface_t = std::ptr::null();
/// # let file_types: *const emf_file_type_span_t = std::ptr::null();
/// assert_ne!(
///     file_handler,
///     std::ptr::null()
/// );
/// assert_ne!(
///     file_types,
///     std::ptr::null()
/// );
/// assert_ne!(
///     unsafe { (*file_types).data },
///     std::ptr::null()
/// );
/// assert!(
///     unsafe { (*file_types).length > 0 }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_fs_register_file_handler(
    file_handler: *const emf_file_handler_interface_t,
    file_types: *const emf_file_type_span_t,
) -> emf_file_handler_t {
    emf_debug_assert_ne!(
        file_handler,
        std::ptr::null(),
        "emf_fs_register_file_handler(): `file_handler` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        file_types,
        std::ptr::null(),
        "emf_fs_register_file_handler(): `file_types` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        (*file_types).data,
        std::ptr::null(),
        "emf_fs_register_file_handler(): `file_types.data` may not be `NULL`."
    );
    emf_debug_assert!(
        (*file_types).length > 0,
        "emf_fs_register_file_handler(): `file_types` must contain at least one element."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .fs_register_file_handler_fn
        .unwrap()(file_handler, file_types)
}

/// Removes a file handler.
///
/// Opened files and mounted paths that are owned by the file handler will be closed/unmounted.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_fs_remove_file_handler(file_handler: emf_file_handler_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_remove_file_handler_fn
        .unwrap()(file_handler)
}

/// Creates a new file.
///
/// Creates a new file with the supplied settings.
///
/// # Options
///
/// Each file handler defines a struct, which specifies how a file is created.
/// The default settings can be used, by passing `std::ptr::null()`.
///
/// The default settings will always create an empty file with write permissions.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::fs::{emf_fs_exists, emf_fs_is_virtual, emf_fs_get_parent, emf_fs_get_access_mode};
/// # let filename: *const emf_path_t = std::ptr::null();
/// assert_ne!(
///     filename,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(filename) },
///     emf_bool_false
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(&emf_fs_get_parent(filename)) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_is_virtual(&emf_fs_get_parent(filename)) },
///     emf_bool_false
/// );
/// assert_eq!(
///     unsafe { emf_fs_get_access_mode(&emf_fs_get_parent(filename)) },
///     emf_file_access_mode_write
/// );
/// ```
///
/// All paths must be normalized.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_fs_create_file(
    filename: *const emf_path_t,
    options: *const ::std::os::raw::c_void,
) {
    emf_debug_assert_ne!(
        filename,
        std::ptr::null(),
        "emf_fs_create_file(): `filename` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_fs_exists(filename),
        emf_bool_false,
        "emf_fs_create_file(): A file already exists at the specified path."
    );
    if cfg!(debug_assertions) {
        let parent = emf_fs_get_parent(filename);

        emf_debug_assert_eq!(
            emf_fs_exists(&parent),
            emf_bool_true,
            "emf_fs_create_file(): The parent directory must already exist."
        );
        emf_debug_assert_eq!(
            emf_fs_is_virtual(&parent),
            emf_bool_false,
            "emf_fs_create_file(): The parent directory must be inside a physical filesystem."
        );
        emf_debug_assert_eq!(
            emf_fs_get_access_mode(&parent),
            emf_file_access_mode_write,
            "emf_fs_create_file(): The parent directory must have write permissions."
        );
    }

    super::S_CORE_INTERFACE.unwrap().fs_create_file_fn.unwrap()(filename, options)
}

/// Creates a new link, connecting to paths.
///
/// A link can only be created, if the source lies
/// in a physical filesystem and the destination lies in the virtual filesystem.
///
/// # Link types
///
/// Currently, the following types are specified:
/// * Symbolic link (`emf_fs_link_symlink`).
/// * Hard link (`emf_fs_link_hardlink`).
///
/// # Symbolic link
///
/// Symbolic links are the lightest type of links.
/// They contain a path to the original entry and can be thought of as a pointer to another path.
/// A side effect of this is, that they can point to an invalid path.
///
/// # Hard link
///
/// Hard links are the heavier counterpart of the symbolic link.
/// They always guarantee that the original entry exists.
/// Deleting the original entry will automatically delete all of its hard links.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::fs::{emf_fs_exists, emf_fs_is_virtual, emf_fs_get_parent, emf_fs_get_access_mode};
/// # let source: *const emf_path_t = std::ptr::null();
/// # let destination: *const emf_path_t = std::ptr::null();
/// assert_ne!(
///     source,
///     std::ptr::null()
/// );
/// assert_ne!(
///     destination,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(source) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(destination) },
///     emf_bool_false
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(&emf_fs_get_parent(destination)) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_is_virtual(source) },
///     emf_bool_false
/// );
/// assert_eq!(
///     unsafe { emf_fs_is_virtual(&emf_fs_get_parent(destination)) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_get_access_mode(&emf_fs_get_parent(destination)) },
///     emf_file_access_mode_write
/// );
/// ```
///
/// All paths must be normalized.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_fs_create_link(
    source: *const emf_path_t,
    destination: *const emf_path_t,
    type_: emf_fs_link_t,
) {
    emf_debug_assert_ne!(
        source,
        std::ptr::null(),
        "emf_fs_create_link(): `source` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        destination,
        std::ptr::null(),
        "emf_fs_create_link(): `destination` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_fs_exists(source),
        emf_bool_true,
        "emf_fs_create_link(): The source must already exist."
    );
    emf_debug_assert_eq!(
        emf_fs_exists(destination),
        emf_bool_false,
        "emf_fs_create_link(): The destination must not already exist."
    );
    emf_debug_assert_eq!(
        emf_fs_is_virtual(source),
        emf_bool_false,
        "emf_fs_create_link(): The source must lie inside a physical filesystem."
    );
    if cfg!(debug_assertions) {
        let destination_parent = emf_fs_get_parent(destination);

        emf_debug_assert_eq!(
            emf_fs_exists(&destination_parent),
            emf_bool_true,
            "emf_fs_create_link(): The parent directory of the destination must already exist."
        );
        emf_debug_assert_eq!(
            emf_fs_is_virtual(&destination_parent),
            emf_bool_false,
            "emf_fs_create_link(): The parent directory of the destination must be inside the virtual filesystem."
        );
        emf_debug_assert_eq!(
            emf_fs_get_access_mode(&destination_parent),
            emf_file_access_mode_write,
            "emf_fs_create_link(): The parent directory of the destination must have write permissions."
        );
    }

    super::S_CORE_INTERFACE.unwrap().fs_create_link_fn.unwrap()(source, destination, type_)
}

/// Creates a new directory.
///
/// Creates a new directory with the supplied settings.
///
/// # Options
///
/// Each file handler defines a struct, which specifies how a directory is created.
/// The default settings can be used, by passing `std::ptr::null()`.
///
/// The default settings will always create an empty directory with write permissions.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::fs::{emf_fs_exists, emf_fs_is_virtual, emf_fs_get_parent, emf_fs_get_access_mode};
/// # let path: *const emf_path_t = std::ptr::null();
/// assert_ne!(
///     path,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(path) },
///     emf_bool_false
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(&emf_fs_get_parent(path)) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_get_access_mode(&emf_fs_get_parent(path)) },
///     emf_file_access_mode_write
/// );
/// ```
///
/// All paths must be normalized.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_fs_create_directory(
    path: *const emf_path_t,
    options: *const ::std::os::raw::c_void,
) {
    emf_debug_assert_ne!(
        path,
        std::ptr::null(),
        "emf_fs_create_directory(): `path` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_fs_exists(path),
        emf_bool_false,
        "emf_fs_create_directory(): The path must not exist."
    );
    if cfg!(debug_assertions) {
        let path_parent = emf_fs_get_parent(path);

        emf_debug_assert_eq!(
            emf_fs_exists(&path_parent),
            emf_bool_true,
            "emf_fs_create_directory(): The parent directory must already exist."
        );
        emf_debug_assert_eq!(
            emf_fs_get_access_mode(&path_parent),
            emf_file_access_mode_write,
            "emf_fs_create_directory(): The parent directory must have write permissions."
        );
    }

    super::S_CORE_INTERFACE
        .unwrap()
        .fs_create_directory_fn
        .unwrap()(path, options)
}

/// Deletes an entry.
///
/// A non empty directory must be deleted with `recursive` set to `emf_bool_true`.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::fs::{emf_fs_exists, emf_fs_can_delete};
/// # let path: *const emf_path_t = std::ptr::null();
/// # let recursive = emf_bool_false;
/// assert_ne!(
///     path,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(path) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_can_delete(path, recursive) },
///     emf_bool_true
/// );
/// ```
///
/// All paths must be normalized.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_fs_delete(path: *const emf_path_t, recursive: emf_bool_t) {
    emf_debug_assert_ne!(
        path,
        std::ptr::null(),
        "emf_fs_delete(): `path` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_fs_exists(path),
        emf_bool_true,
        "emf_fs_delete(): The path must already exist."
    );
    emf_debug_assert_eq!(
        emf_fs_can_delete(path, recursive),
        emf_bool_true,
        "emf_fs_delete(): The path must be deletable."
    );

    super::S_CORE_INTERFACE.unwrap().fs_delete_fn.unwrap()(path, recursive)
}

/// Mounts a memory buffer.
///
/// # Options
///
/// Each file handler defines a struct, which specifies how a file is mounted.
/// The default settings can be used, by passing `std::ptr::null()`.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::fs::{emf_fs_exists, emf_fs_get_parent, emf_fs_is_virtual, emf_fs_get_access_mode};
/// # let file: *const emf_memory_span_t = std::ptr::null();
/// # let mount_point: *const emf_path_t = std::ptr::null();
/// assert_ne!(
///     file,
///     std::ptr::null()
/// );
/// assert_ne!(
///     mount_point,
///     std::ptr::null()
/// );
/// assert_ne!(
///     unsafe { (*file).data },
///     std::ptr::null()
/// );
/// assert!(
///     unsafe { (*file).length > 0}
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(mount_point) },
///     emf_bool_false
/// );
/// assert_eq!(
///     unsafe { emf_fs_exists(&emf_fs_get_parent(mount_point)) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_is_virtual(&emf_fs_get_parent(mount_point)) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_fs_get_access_mode(&emf_fs_get_parent(mount_point)) },
///     emf_file_access_mode_write
/// );
/// ```
///
/// All paths must be normalized.
/// Furthermore, the file handler must be able to mount the type of the supplied buffer.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_fs_mount_memory_file(
    file_handler: emf_file_handler_t,
    file: *const emf_memory_span_t,
    access_mode: emf_access_mode_t,
    mount_point: *const emf_path_t,
    options: *const ::std::os::raw::c_void,
) -> emf_mount_id_t {
    emf_debug_assert_ne!(
        file,
        std::ptr::null(),
        "emf_fs_mount_memory_file(): `file` may not be `NULL`"
    );
    emf_debug_assert_ne!(
        mount_point,
        std::ptr::null(),
        "emf_fs_mount_memory_file(): `mount_point` may not be `NULL`"
    );
    emf_debug_assert_ne!(
        (*file).data,
        std::ptr::null(),
        "emf_fs_mount_memory_file(): `file.data` may not be `NULL`"
    );
    emf_debug_assert!(
        (*file).length > 0,
        "emf_fs_mount_memory_file(): The file may not be empty."
    );
    emf_debug_assert_eq!(
        emf_fs_exists(mount_point),
        emf_bool_false,
        "emf_fs_mount_memory_file(): The mount point must not already exist."
    );
    if cfg!(debug_assertions) {
        let mount_point_parent = emf_fs_get_parent(mount_point);

        emf_debug_assert_eq!(
            emf_fs_exists(&mount_point_parent),
            emf_bool_true,
            "emf_fs_mount_memory_file(): The parent directory of the mount point must already exist."
        );
        emf_debug_assert_eq!(
            emf_fs_is_virtual(&mount_point_parent),
            emf_bool_true,
            "emf_fs_mount_memory_file(): The parent directory of the mount point must be virtual."
        );
        emf_debug_assert_eq!(
            emf_fs_get_access_mode(&mount_point_parent),
            emf_file_access_mode_write,
            "emf_fs_mount_memory_file(): The parent directory of the mount point must have write permissions."
        );
    }

    super::S_CORE_INTERFACE
        .unwrap()
        .fs_mount_memory_file_fn
        .unwrap()(file_handler, file, access_mode, mount_point, options)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_mount_native_path(
    file_handler: emf_file_handler_t,
    path: *const emf_native_path_char_t,
    access_mode: emf_access_mode_t,
    mount_point: *const emf_path_t,
    options: *const ::std::os::raw::c_void,
) -> emf_mount_id_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_mount_native_path_fn
        .unwrap()(file_handler, path, access_mode, mount_point, options)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unmount(mount_id: emf_mount_id_t) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().fs_unmount_fn.unwrap()(mount_id)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_set_access_mode(
    path: *const emf_path_t,
    access_mode: emf_access_mode_t,
    recursive: emf_bool_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_set_access_mode_fn
        .unwrap()(path, access_mode, recursive)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_access_mode(path: *const emf_path_t) -> emf_access_mode_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_access_mode_fn
        .unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_mount_id(path: *const emf_path_t) -> emf_mount_id_t {
    super::S_CORE_INTERFACE.unwrap().fs_get_mount_id_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_can_access(
    path: *const emf_path_t,
    access_mode: emf_access_mode_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().fs_can_access_fn.unwrap()(path, access_mode)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_can_set_access_mode(
    path: *const emf_path_t,
    access_mode: emf_access_mode_t,
    recursive: emf_bool_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_can_set_access_mode_fn
        .unwrap()(path, access_mode, recursive)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_is_virtual(path: *const emf_path_t) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().fs_is_virtual_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_can_delete(
    path: *const emf_path_t,
    recursive: emf_bool_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().fs_can_delete_fn.unwrap()(path, recursive)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_can_mount_type(
    file_handler: emf_file_handler_t,
    type_: *const emf_file_type_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_can_mount_type_fn
        .unwrap()(file_handler, type_)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_can_mount_native_path(
    file_handler: emf_file_handler_t,
    path: *const emf_native_path_char_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_can_mount_native_path_fn
        .unwrap()(file_handler, path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_num_entries(
    path: *const emf_path_t,
    recursive: emf_bool_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_num_entries_fn
        .unwrap()(path, recursive)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_entries(
    path: *const emf_path_t,
    recursive: emf_bool_t,
    buffer: *mut emf_path_span_t,
) -> usize {
    super::S_CORE_INTERFACE.unwrap().fs_get_entries_fn.unwrap()(path, recursive, buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_exists(path: *const emf_path_t) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().fs_exists_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_type_exists(type_: *const emf_file_type_t) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().fs_type_exists_fn.unwrap()(type_)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_entry_type(path: *const emf_path_t) -> emf_fs_entry_type_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_entry_type_fn
        .unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_resolve_link(path: *const emf_path_t) -> emf_path_t {
    super::S_CORE_INTERFACE.unwrap().fs_resolve_link_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_link_type(path: *const emf_path_t) -> emf_fs_link_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_link_type_fn
        .unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_size(path: *const emf_path_t) -> emf_entry_size_t {
    super::S_CORE_INTERFACE.unwrap().fs_get_size_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_native_path_length(path: *const emf_path_t) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_native_path_length_fn
        .unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_native_path(
    path: *const emf_path_t,
    buffer: *mut emf_native_path_span_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_native_path_fn
        .unwrap()(path, buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_num_file_handlers() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_num_file_handlers_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_file_handlers(buffer: *mut emf_file_handler_span_t) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_file_handlers_fn
        .unwrap()(buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_file_handler_from_type(
    type_: *const emf_file_type_t,
) -> emf_file_handler_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_file_handler_from_type_fn
        .unwrap()(type_)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_num_file_types() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_num_file_types_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_file_types(buffer: *mut emf_file_type_span_t) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_file_types_fn
        .unwrap()(buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_num_handler_file_types(
    file_handler: emf_file_handler_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_num_handler_file_types_fn
        .unwrap()(file_handler)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_handler_file_types(
    file_handler: emf_file_handler_t,
    buffer: *mut emf_file_type_span_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_get_handler_file_types_fn
        .unwrap()(file_handler, buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_normalize(path: *const emf_path_t) -> emf_path_t {
    super::S_CORE_INTERFACE.unwrap().fs_normalize_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_get_parent(path: *const emf_path_t) -> emf_path_t {
    super::S_CORE_INTERFACE.unwrap().fs_get_parent_fn.unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_join(lhs: *const emf_path_t, rhs: *const emf_path_t) -> emf_path_t {
    super::S_CORE_INTERFACE.unwrap().fs_join_fn.unwrap()(lhs, rhs)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_create_mount_id(
    mount_point: *const emf_path_t,
) -> emf_mount_id_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_create_mount_id_fn
        .unwrap()(mount_point)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_remove_mount_id(mount_id: emf_mount_id_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_remove_mount_id_fn
        .unwrap()(mount_id)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_unmount_force(mount_id: emf_mount_id_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_unmount_force_fn
        .unwrap()(mount_id)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_link_mount_point(
    mount_id: emf_mount_id_t,
    file_handler: emf_file_handler_t,
    file_handler_mount_id: emf_file_handler_mount_id_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_link_mount_point_fn
        .unwrap()(mount_id, file_handler, file_handler_mount_id)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_create_file_stream() -> emf_file_stream_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_create_file_stream_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_remove_file_stream(file_stream: emf_file_stream_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_remove_file_stream_fn
        .unwrap()(file_stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_link_file_stream(
    file_stream: emf_file_stream_t,
    file_handler: emf_file_handler_t,
    file_handler_stream: emf_file_handler_stream_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_link_file_stream_fn
        .unwrap()(file_stream, file_handler, file_handler_stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_get_file_handler_handle_from_stream(
    stream: emf_file_stream_t,
) -> emf_file_handler_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_get_file_handler_handle_from_stream_fn
        .unwrap()(stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_get_file_handler_handle_from_path(
    path: *const emf_path_t,
) -> emf_file_handler_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_get_file_handler_handle_from_path_fn
        .unwrap()(path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_get_file_handler_stream(
    file_stream: emf_file_stream_t,
) -> emf_file_handler_stream_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_get_file_handler_stream_fn
        .unwrap()(file_stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_get_file_handler_mount_id(
    mount_id: emf_mount_id_t,
) -> emf_file_handler_mount_id_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_get_file_handler_mount_id_fn
        .unwrap()(mount_id)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_unsafe_get_file_handler(
    file_handler: emf_file_handler_t,
) -> emf_file_handler_interface_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_unsafe_get_file_handler_fn
        .unwrap()(file_handler)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_open(
    filename: *const emf_path_t,
    open_mode: emf_file_open_mode_t,
    access_mode: emf_access_mode_t,
    options: *const ::std::os::raw::c_void,
) -> emf_file_stream_t {
    super::S_CORE_INTERFACE.unwrap().fs_stream_open_fn.unwrap()(
        filename,
        open_mode,
        access_mode,
        options,
    )
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_close(stream: emf_file_stream_t) {
    super::S_CORE_INTERFACE.unwrap().fs_stream_close_fn.unwrap()(stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_flush(stream: emf_file_stream_t) {
    super::S_CORE_INTERFACE.unwrap().fs_stream_flush_fn.unwrap()(stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_read(
    stream: emf_file_stream_t,
    buffer: *mut emf_fs_buffer_t,
    read_count: usize,
) -> usize {
    super::S_CORE_INTERFACE.unwrap().fs_stream_read_fn.unwrap()(stream, buffer, read_count)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_write(
    stream: emf_file_stream_t,
    buffer: *const emf_fs_buffer_t,
    write_count: usize,
) -> usize {
    super::S_CORE_INTERFACE.unwrap().fs_stream_write_fn.unwrap()(stream, buffer, write_count)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_get_pos(stream: emf_file_stream_t) -> emf_pos_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_stream_get_pos_fn
        .unwrap()(stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_set_pos(
    stream: emf_file_stream_t,
    position: emf_pos_t,
) -> emf_off_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_stream_set_pos_fn
        .unwrap()(stream, position)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_move_pos(
    stream: emf_file_stream_t,
    offset: emf_off_t,
    direction: emf_fs_direction_t,
) -> emf_off_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_stream_move_pos_fn
        .unwrap()(stream, offset, direction)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_can_write(stream: emf_file_stream_t) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_stream_can_write_fn
        .unwrap()(stream)
}

#[no_mangle]
pub unsafe extern "C" fn emf_fs_stream_can_grow(
    stream: emf_file_stream_t,
    size: usize,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .fs_stream_can_grow_fn
        .unwrap()(stream, size)
}
