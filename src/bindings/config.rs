//! Bindings for the `config` system.
//!
//! The `config` system consists of namespaces and properties.
//! Namespaces can contain properties and other namespaces. Properties are homogenous arrays of values.
//! A namespace- or property-name must be a `UTF-8` string.
//! All Namespaces are implicitly nested inside the global namespace.
//!
//! # Properties
//!
//! Properties can have the following types:
//! * Bool ([`emf_bool_t`](../../type.emf_bool_t.html)).
//! * Integer (`i64`).
//! * Reals (`f64`).
//! * Strings ([`*mut c_char`](https://doc.rust-lang.org/nightly/std/os/raw/type.c_char.html)).
//!
//! Strings are treated as a sized blob of data, and as such can contain values other than strings.
//! Unless stated otherwise, a string property represents a `UTF-8` string
//! without a terminating `\0` character.
//!
//! The array length of a property can not be changed after it's construction.
//! Additionally, String properties are assigned a maximum length at construction.
use crate::*;

/// Creates a new namespace.
///
/// Creates a new namespace, which can hold properties.
/// Multiple namespaces can be nested inside each other.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists};
/// # let group = std::ptr::null();
/// assert_ne!(
///     group,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_namespace_exists(group) },
///     emf_bool_false
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_add_namespace(group: *const emf_config_namespace_t) {
    emf_debug_assert_ne!(
        group,
        std::ptr::null(),
        "emf_config_add_namespace(): `group` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_namespace_exists(group),
        emf_bool_false,
        "emf_config_add_namespace(): The namespace may not already exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_add_namespace_fn
        .unwrap()(group)
}

/// Removes an existing namespace.
///
/// Removes a namespace, its properties and its child-namespaces.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists};
/// # let group = std::ptr::null();
/// assert_ne!(
///     group,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_namespace_exists(group) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_remove_namespace(group: *const emf_config_namespace_t) {
    emf_debug_assert_ne!(
        group,
        std::ptr::null(),
        "emf_config_remove_namespace(): `group` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_namespace_exists(group),
        emf_bool_true,
        "emf_config_remove_namespace(): The namespace must already exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_remove_namespace_fn
        .unwrap()(group)
}

/// Adds a boolean property to a namespace.
///
/// Adds a boolean property to the supplied namespace.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let array_size = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_false
/// );
/// assert!(
///     array_size >= 1
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_add_property_boolean(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    array_size: usize,
    default_value: emf_bool_t,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_add_property_boolean(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_add_property_boolean(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_false,
        "emf_config_add_property_boolean(): The property may not already exist."
    );
    emf_debug_assert!(
        array_size >= 1,
        "emf_config_add_property_boolean(): The array must have a minimum length of 1."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_add_property_boolean_fn
        .unwrap()(group, property, array_size, default_value)
}

/// Adds an integer property to a namespace.
///
/// Adds an integer property to the supplied namespace.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let array_size = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_false
/// );
/// assert!(
///     array_size >= 1
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_add_property_integer(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    array_size: usize,
    default_value: i64,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_add_property_integer(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_add_property_integer(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_false,
        "emf_config_add_property_integer(): The property may not already exist."
    );
    emf_debug_assert!(
        array_size >= 1,
        "emf_config_add_property_integer(): The array must have a minimum length of 1."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_add_property_integer_fn
        .unwrap()(group, property, array_size, default_value)
}

/// Adds a real property to a namespace.
///
/// Adds a real property to the supplied namespace.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let array_size = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_false
/// );
/// assert!(
///     array_size >= 1
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_add_property_real(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    array_size: usize,
    default_value: f64,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_add_property_real(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_add_property_real(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_false,
        "emf_config_add_property_real(): The property may not already exist."
    );
    emf_debug_assert!(
        array_size >= 1,
        "emf_config_add_property_real(): The array must have a minimum length of 1."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_add_property_real_fn
        .unwrap()(group, property, array_size, default_value)
}

/// Adds a string property to a namespace.
///
/// Adds a string property to the supplied namespace.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let array_size = 0;
/// # let string_length = 0;
/// # let default_value: *const emf_config_string_t = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_false
/// );
/// assert!(
///     array_size >= 1
/// );
/// assert!(
///     string_length >= 1
/// );
/// assert_ne!(
///     default_value,
///     std::ptr::null()
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_add_property_string(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    array_size: usize,
    string_length: usize,
    default_value: *const emf_config_string_t,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_add_property_string(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_add_property_string(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_false,
        "emf_config_add_property_string(): The property may not already exist."
    );
    emf_debug_assert!(
        array_size >= 1,
        "emf_config_add_property_string(): The array must have a minimum length of 1."
    );
    emf_debug_assert!(
        string_length >= 1,
        "emf_config_add_property_string(): The string must have a minimum length of 1."
    );
    emf_debug_assert_ne!(
        default_value,
        std::ptr::null(),
        "emf_config_add_property_string(): `default_value` may not be `NULL`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_add_property_string_fn
        .unwrap()(group, property, array_size, string_length, default_value)
}

/// Removes a property.
///
/// Removes a property from the supplied namespace.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_remove_property(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_remove_property(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_remove_property(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_remove_property(): The property must already exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_remove_property_fn
        .unwrap()(group, property)
}

/// Returns the number of namespaces.
///
/// Returns the number of namespaces inside (and including) the supplied namespace.
/// The namespaces can be counted recursively by setting `recursive` to `emf_bool_true`.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists};
/// # let group = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_num_namespaces(
    group: *const emf_config_namespace_t,
    recursive: emf_bool_t,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_num_namespaces(): The namespace must refer to the global namespace or exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_num_namespaces_fn
        .unwrap()(group, recursive)
}

/// Copies all namespaces into a buffer.
///
/// Copies every namespace inside (and including) the supplied namespace into the supplies `buffer`.
/// The namespaces can be copied recursively by setting `recursive` to `emf_bool_true`.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_get_num_namespaces};
/// # let group = std::ptr::null();
/// # let recursive = emf_bool_false;
/// # let buffer: *mut emf_config_namespace_span_t = std::ptr::null_mut();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     buffer,
///     std::ptr::null_mut()
/// );
/// assert_ne!(
///     unsafe { (*buffer).data },
///     std::ptr::null_mut()
/// );
/// assert!(
///     unsafe { (*buffer).length >=  emf_config_get_num_namespaces(group, recursive) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_namespaces(
    group: *const emf_config_namespace_t,
    recursive: emf_bool_t,
    buffer: *mut emf_config_namespace_span_t,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_num_namespaces(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        buffer,
        std::ptr::null_mut(),
        "emf_config_get_num_namespaces(): `buffer` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        (*buffer).data,
        std::ptr::null_mut(),
        "emf_config_get_num_namespaces(): `buffer.data` may not be `NULL`."
    );
    emf_debug_assert!(
        (*buffer).length >= emf_config_get_num_namespaces(group, recursive),
        "emf_config_get_num_namespaces(): The buffer must be big enough to contain all namespaces."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_namespaces_fn
        .unwrap()(group, recursive, buffer)
}

/// Retrieves the number of properties in a namespace.
///
/// Returns the number of properties inside the supplied namespace.
/// The properties can be counted recursively by setting `recursive` to `emf_bool_true`.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists};
/// # let group = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_num_properties(
    group: *const emf_config_namespace_t,
    recursive: emf_bool_t,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_num_properties(): The namespace must refer to the global namespace or exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_num_properties_fn
        .unwrap()(group, recursive)
}

/// Copies all properties into a buffer.
///
/// Copies the properties inside the supplied namespace into `buffer`.
/// The properties can be copied recursively by setting `recursive` to `emf_bool_true`.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_get_num_properties};
/// # let group = std::ptr::null();
/// # let recursive = emf_bool_false;
/// # let buffer: *mut emf_config_property_info_span_t = std::ptr::null_mut();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     buffer,
///     std::ptr::null_mut()
/// );
/// assert_ne!(
///     unsafe { (*buffer).data },
///     std::ptr::null_mut()
/// );
/// assert!(
///     unsafe { (*buffer).length >= emf_config_get_num_properties(group, recursive) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_properties(
    group: *const emf_config_namespace_t,
    recursive: emf_bool_t,
    buffer: *mut emf_config_property_info_span_t,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_properties(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        buffer,
        std::ptr::null_mut(),
        "emf_config_get_properties(): `buffer` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        (*buffer).data,
        std::ptr::null_mut(),
        "emf_config_get_properties(): `buffer.data` may not be `NULL`."
    );
    emf_debug_assert!(
        (*buffer).length >= emf_config_get_num_properties(group, recursive),
        "emf_config_get_properties(): The buffer must be big enough to contain all properties."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_properties_fn
        .unwrap()(group, recursive, buffer)
}

/// Check if a namespace exists.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # let group: *const emf_config_namespace_t = std::ptr::null();
/// assert_ne!(
///     group,
///     std::ptr::null()
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_namespace_exists(group: *const emf_config_namespace_t) -> emf_bool_t {
    emf_debug_assert_ne!(
        group,
        std::ptr::null(),
        "emf_config_namespace_exists(): `group` may not be `NULL`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_namespace_exists_fn
        .unwrap()(group)
}

/// Checks if a property exists.
///
/// Checks if the property is present inside the supplied namespace.
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists};
/// # let group = std::ptr::null();
/// # let property: *const emf_config_property_t = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_exists(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
) -> emf_bool_t {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_exists(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_exists(): `property` may not be `NULL`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_exists_fn
        .unwrap()(group, property)
}

/// Retrieves the type of a property.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_property_type(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
) -> emf_config_property_type_t {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_property_type(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_get_property_type(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_get_property_type(): The property must exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_property_type_fn
        .unwrap()(group, property)
}

/// Retrieves the array length of a property.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_property_array_size(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_property_array_size(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_get_property_array_size(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_get_property_array_size(): The property must exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_property_array_size_fn
        .unwrap()(group, property)
}

/// Retrieves the string length of a property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_string
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_property_string_size(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_property_string_size(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_get_property_string_size(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_get_property_string_size(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_string,
        "emf_config_get_property_string_size(): The property must be a string."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_get_property_string_size(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_property_string_size_fn
        .unwrap()(group, property, index)
}

/// Retrieves the maximum string length of a property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_string
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_get_property_string_max_size(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
) -> usize {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_get_property_string_max_size(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_get_property_string_max_size(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_get_property_string_max_size(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_string,
        "emf_config_get_property_string_max_size(): The property must be a string."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_get_property_string_max_size_fn
        .unwrap()(group, property)
}

/// Reads the value of a bool property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_bool
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_read_bool(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
) -> emf_bool_t {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_read_bool(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_read_bool(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_read_bool(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_bool,
        "emf_config_property_read_bool(): The property must be a bool."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_read_bool(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_read_bool_fn
        .unwrap()(group, property, index)
}

/// Reads the value of an integer property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_integer
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_read_integer(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
) -> i64 {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_read_integer(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_read_integer(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_read_integer(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_integer,
        "emf_config_property_read_integer(): The property must be an integer."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_read_integer(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_read_integer_fn
        .unwrap()(group, property, index)
}

/// Reads the value of a real property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_real
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_read_real(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
) -> f64 {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_read_real(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_read_real(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_read_real(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_real,
        "emf_config_property_read_real(): The property must be a real value."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_read_real(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_read_real_fn
        .unwrap()(group, property, index)
}

/// Reads the value of a string property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size, emf_config_get_property_string_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// # let buffer: *mut emf_config_string_t = std::ptr::null_mut();
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_ne!(
///     buffer,
///     std::ptr::null_mut()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_string
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// assert_ne!(
///     unsafe { (*buffer).data },
///     std::ptr::null_mut()
/// );
/// assert!(
///     unsafe { (*buffer).length >= emf_config_get_property_string_size(group, property, index) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_read_string(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
    buffer: *mut emf_config_string_t,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_read_string(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_read_string(): `property` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        buffer,
        std::ptr::null_mut(),
        "emf_config_property_read_string(): `buffer` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_read_string(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_string,
        "emf_config_property_read_string(): The property must be a string."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_read_string(): The index is out of bounds."
    );
    emf_debug_assert_ne!(
        (*buffer).data,
        std::ptr::null_mut(),
        "emf_config_property_read_string(): `buffer.data` may not be `NULL`."
    );
    emf_debug_assert!(
        (*buffer).length >= emf_config_get_property_string_size(group, property, index),
        "emf_config_property_read_string(): The buffer must be big enough to contain the entire string."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_read_string_fn
        .unwrap()(group, property, index, buffer)
}

/// Writes a value into a bool property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_bool
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_write_bool(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
    value: emf_bool_t,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_write_bool(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_write_bool(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_write_bool(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_bool,
        "emf_config_property_write_bool(): The property must be a bool."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_write_bool(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_write_bool_fn
        .unwrap()(group, property, index, value)
}

/// Writes a value into an integer property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_integer
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_write_integer(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
    value: i64,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_write_integer(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_write_integer(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_write_integer(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_integer,
        "emf_config_property_write_integer(): The property must be an integer."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_write_integer(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_write_integer_fn
        .unwrap()(group, property, index, value)
}

/// Writes a value into a real property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_real
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_write_real(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
    value: f64,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_write_real(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_write_real(): `property` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_write_real(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_real,
        "emf_config_property_write_real(): The property must be a real value."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_write_real(): The index is out of bounds."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_write_real_fn
        .unwrap()(group, property, index, value)
}

/// Writes a value into a string property member.
///
/// The namespace `std::ptr::null()` and the empty namespace refer to the global namespace.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::config::{emf_config_namespace_exists, emf_config_property_exists, emf_config_get_property_type, emf_config_get_property_array_size};
/// # let group = std::ptr::null();
/// # let property = std::ptr::null();
/// # let buffer: *const emf_config_string_t = std::ptr::null();
/// # let index = 0;
/// assert!(
///     group == std::ptr::null() || unsafe { emf_config_namespace_exists(group) == emf_bool_true }
/// );
/// assert_ne!(
///     property,
///     std::ptr::null()
/// );
/// assert_ne!(
///     buffer,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_config_property_exists(group, property) },
///     emf_bool_true
/// );
/// assert_eq!(
///     unsafe { emf_config_get_property_type(group, property) },
///     emf_config_prop_type_string
/// );
/// assert!(
///     unsafe { index < emf_config_get_property_array_size(group, property) }
/// );
/// assert_ne!(
///     unsafe { (*buffer).data },
///     std::ptr::null()
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe fn emf_config_property_write_string(
    group: *const emf_config_namespace_t,
    property: *const emf_config_property_t,
    index: usize,
    buffer: *const emf_config_string_t,
) {
    emf_debug_assert!(
        group == std::ptr::null() || emf_config_namespace_exists(group) == emf_bool_true,
        "emf_config_property_write_string(): The namespace must refer to the global namespace or exist."
    );
    emf_debug_assert_ne!(
        property,
        std::ptr::null(),
        "emf_config_property_write_string(): `property` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        buffer,
        std::ptr::null(),
        "emf_config_property_write_string(): `buffer` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_config_property_exists(group, property),
        emf_bool_true,
        "emf_config_property_write_string(): The property must exist."
    );
    emf_debug_assert_eq!(
        emf_config_get_property_type(group, property),
        emf_config_prop_type_string,
        "emf_config_property_write_string(): The property must be a string."
    );
    emf_debug_assert!(
        index < emf_config_get_property_array_size(group, property),
        "emf_config_property_write_string(): The index is out of bounds."
    );
    emf_debug_assert_ne!(
        (*buffer).data,
        std::ptr::null_mut(),
        "emf_config_property_write_string(): `buffer.data` may not be `NULL`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .config_property_write_string_fn
        .unwrap()(group, property, index, buffer)
}
