use crate::*;
use lazy_static::lazy_static;

static mut S_CORE_INTERFACE: Option<&'static emf_core_interface_t> = None;

lazy_static! {
    static ref S_CORE_INTERFACE_INFO: emf_interface_info_t = {
        unsafe {
            let mut interface_info: emf_interface_info_t = { std::mem::zeroed() };

            let interface_name_slice: &[std::os::raw::c_char] =
                std::mem::transmute(&EMF_CORE_INTERFACE_NAME[..EMF_CORE_INTERFACE_NAME.len() - 1]);

            interface_info.name.data[..interface_name_slice.len()]
                .copy_from_slice(interface_name_slice);
            interface_info.name.length = interface_name_slice.len();

            interface_info.version.major = EMF_CORE_VERSION_MAJOR as i32;
            interface_info.version.minor = EMF_CORE_VERSION_MINOR as i32;
            interface_info.version.patch = EMF_CORE_VERSION_PATCH as i32;
            interface_info.version.build_number = EMF_CORE_BUILD as i64;
            interface_info.version.release_number = EMF_CORE_PRE_RELEASE_NUMBER as i8;
            interface_info.version.release_type =
                EMF_CORE_PRE_RELEASE_TYPE as emf_version_release_t;

            interface_info
        }
    };
    static ref S_INITIALIZATION_ERROR_MSG: &'static std::ffi::CStr = {
        static mut ERROR: [u8; 256] = [0; 256];

        static ERROR_BEGIN: &'static [u8] = b"Could not initialize the bindings to the '";
        static ERROR_END: &'static [u8] = b"' interface.\0";

        let interface_name_slice = &EMF_CORE_INTERFACE_NAME[..EMF_CORE_INTERFACE_NAME.len() - 1];

        unsafe {
            ERROR[..ERROR_BEGIN.len()].copy_from_slice(ERROR_BEGIN);
            ERROR[ERROR_BEGIN.len()..ERROR_BEGIN.len() + interface_name_slice.len()]
                .copy_from_slice(interface_name_slice);
            ERROR[ERROR_BEGIN.len() + interface_name_slice.len()
                ..ERROR_BEGIN.len() + interface_name_slice.len() + ERROR_END.len()]
                .copy_from_slice(ERROR_END);

            std::ffi::CStr::from_bytes_with_nul_unchecked(
                &ERROR[..ERROR_BEGIN.len() + interface_name_slice.len() + ERROR_END.len() + 1],
            )
        }
    };
}

pub mod config;
pub mod core;
pub mod event;
pub mod fs;
pub mod library;
pub mod module;
pub mod version;

#[test]
fn test_core_interface_info_value() {
    assert_eq!(
        S_CORE_INTERFACE_INFO.name.length,
        EMF_CORE_INTERFACE_NAME.len() - 1
    );
    assert_eq!(
        std::str::from_utf8(unsafe {
            std::mem::transmute(
                &S_CORE_INTERFACE_INFO.name.data[..S_CORE_INTERFACE_INFO.name.length],
            )
        })
        .unwrap(),
        std::str::from_utf8(&EMF_CORE_INTERFACE_NAME[..EMF_CORE_INTERFACE_NAME.len() - 1]).unwrap()
    );

    assert_eq!(
        S_CORE_INTERFACE_INFO.version.major as u32,
        EMF_CORE_VERSION_MAJOR
    );
    assert_eq!(
        S_CORE_INTERFACE_INFO.version.minor as u32,
        EMF_CORE_VERSION_MINOR
    );
    assert_eq!(
        S_CORE_INTERFACE_INFO.version.patch as u32,
        EMF_CORE_VERSION_PATCH
    );
    assert_eq!(
        S_CORE_INTERFACE_INFO.version.build_number as u32,
        EMF_CORE_BUILD
    );
    assert_eq!(
        S_CORE_INTERFACE_INFO.version.release_number as u32,
        EMF_CORE_PRE_RELEASE_NUMBER
    );
    assert_eq!(
        S_CORE_INTERFACE_INFO.version.release_type as u32,
        EMF_CORE_PRE_RELEASE_TYPE
    );
}

#[test]
fn test_initialization_error_msg_value() {
    let emf_core_interface_name =
        std::str::from_utf8(&EMF_CORE_INTERFACE_NAME[..EMF_CORE_INTERFACE_NAME.len() - 1]).unwrap();

    let error_msg = S_INITIALIZATION_ERROR_MSG.to_str().unwrap();
    let expected_msg = format!(
        "Could not initialize the bindings to the '{}' interface.\0",
        emf_core_interface_name
    );

    assert_eq!(error_msg, expected_msg.as_str());
}

pub unsafe fn initialize_bindings(get_function_fn: emf_get_function_fn_t) {
    let get_function_fn = get_function_fn.unwrap();

    let panic_fn =
        std::mem::transmute::<emf_fn_t, emf_panic_fn_t>(get_function_fn(emf_fn_ptr_id_panic))
            .unwrap();

    let module_get_interface_handle_fn =
        std::mem::transmute::<emf_fn_t, emf_module_get_interface_handle_fn_t>(get_function_fn(
            emf_fn_ptr_id_module_get_interface_handle,
        ))
        .unwrap();

    let module_get_interface_fn = std::mem::transmute::<emf_fn_t, emf_module_get_interface_fn_t>(
        get_function_fn(emf_fn_ptr_id_module_get_interface),
    )
    .unwrap();

    let mut interface_buffer: emf_interface_t = { std::mem::zeroed() };

    let interface_handle = module_get_interface_handle_fn(&*S_CORE_INTERFACE_INFO);

    if module_get_interface_fn(
        interface_handle,
        &*S_CORE_INTERFACE_INFO,
        &mut interface_buffer,
    ) == emf_error_none
    {
        S_CORE_INTERFACE =
            (interface_buffer.interface as *mut emf_core_interface_t).as_ref::<'static>();
    } else {
        panic_fn(S_INITIALIZATION_ERROR_MSG.as_ptr());
        unreachable!();
    }
}
