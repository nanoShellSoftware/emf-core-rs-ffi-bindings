//! Bindings for the `event` system.
//!
//! The `event` system provides an interface
//! to the creation and management of events.
//!
//! # Events
//!
//! Events come in two varieties:
//! * Public (named).
//! * Private (unnamed).
//!
//! The name of a public event must be encoded as `UTF-8`.
use crate::*;

/// Creates a new public event.
///
/// Creates a new public event with the name `event_name`.
/// An empty event can be created, by passing `None` to `event_handler`.
///
/// # Naming convention
///
/// Events follow a similar naming convention to the one found in the `C` language:
/// * Names starting with `__` are reserved for the `emf-core` interface.
/// * Names starting with `_Interface` (where `Interface` is `I` followed by the name, e.g. `_Iemf::core`)
///   are reserved for the corresponding interface.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_name_exists};
/// # let event_name = std::ptr::null();
/// assert_ne!(
///     event_name,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_event_name_exists(event_name) },
///     emf_bool_false
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_create(
    event_name: *const emf_event_name_t,
    event_handler: emf_event_handler_fn_t,
) -> emf_event_handle_t {
    emf_debug_assert_ne!(
        event_name,
        std::ptr::null(),
        "emf_event_create(): `event_name` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_event_name_exists(event_name),
        emf_bool_false,
        "emf_event_create(): The event name must not already exist."
    );

    super::S_CORE_INTERFACE.unwrap().event_create_fn.unwrap()(event_name, event_handler)
}

/// Creates a new private event.
///
/// Private events can only be accessed by the handle.
/// An empty event can be created, by passing `None` to `event_handler`.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_event_create_private(
    event_handler: emf_event_handler_fn_t,
) -> emf_event_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .event_create_private_fn
        .unwrap()(event_handler)
}

/// Destroys an event.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_handle_exists};
/// # let event_handle = Default::default();
/// assert_eq!(
///     unsafe { emf_event_handle_exists(event_handle) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_destroy(event_handle: emf_event_handle_t) {
    emf_debug_assert_eq!(
        emf_event_handle_exists(event_handle),
        emf_bool_true,
        "emf_event_destroy(): The event must exist."
    );

    super::S_CORE_INTERFACE.unwrap().event_destroy_fn.unwrap()(event_handle)
}

/// Converts a private event to a public event.
///
/// The conversion is applied, by assigning a name to the private event. <br>
/// See [`emf_event_create()`](./fn.emf_event_create.html) for more information.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_handle_exists, emf_event_name_exists};
/// # let event_handle = Default::default();
/// # let event_name = std::ptr::null();
/// assert_eq!(
///     unsafe { emf_event_handle_exists(event_handle) },
///     emf_bool_true
/// );
/// assert_ne!(
///     event_name,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_event_name_exists(event_name) },
///     emf_bool_false
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_publish(
    event_handle: emf_event_handle_t,
    event_name: *const emf_event_name_t,
) {
    emf_debug_assert_eq!(
        emf_event_handle_exists(event_handle),
        emf_bool_true,
        "emf_event_publish(): The event must already exist."
    );
    emf_debug_assert_ne!(
        event_name,
        std::ptr::null(),
        "emf_event_publish(): `event_name` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_event_name_exists(event_name),
        emf_bool_false,
        "emf_event_publish(): The name must not already exist."
    );

    super::S_CORE_INTERFACE.unwrap().event_publish_fn.unwrap()(event_handle, event_name)
}

/// Returns the number of public events.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_event_get_num_public_events() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .event_get_num_public_events_fn
        .unwrap()()
}

/// Copies all public events into a buffer.
///
/// The names of all public events are copied into `buffer`.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_get_num_public_events};
/// # let buffer: *mut emf_event_name_span_t = std::ptr::null_mut();
/// assert_ne!(
///     buffer,
///     std::ptr::null_mut()
/// );
/// assert_ne!(
///     unsafe { (*buffer).data },
///     std::ptr::null_mut()
/// );
/// assert!(
///     unsafe { (*buffer).length >= emf_event_get_num_public_events() }
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_get_public_events(buffer: *mut emf_event_name_span_t) -> usize {
    emf_debug_assert_ne!(
        buffer,
        std::ptr::null_mut(),
        "emf_event_get_public_events(): `buffer` may not be `NULL`."
    );
    emf_debug_assert_ne!(
        (*buffer).data,
        std::ptr::null_mut(),
        "emf_event_get_public_events(): `buffer.data` may not be `NULL`."
    );
    emf_debug_assert!(
        (*buffer).length >= emf_event_get_num_public_events(),
        "emf_event_get_public_events(): `buffer` must be big enough to contain all the public events."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .event_get_public_events_fn
        .unwrap()(buffer)
}

/// Fetches the handle of a public event.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_name_exists};
/// # let event_name = std::ptr::null();
/// assert_ne!(
///     event_name,
///     std::ptr::null()
/// );
/// assert_eq!(
///     unsafe { emf_event_name_exists(event_name) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_get_event_handle(
    event_name: *const emf_event_name_t,
) -> emf_event_handle_t {
    emf_debug_assert_ne!(
        event_name,
        std::ptr::null(),
        "emf_event_get_event_handle(): `event_name` may not be `NULL`."
    );
    emf_debug_assert_eq!(
        emf_event_name_exists(event_name),
        emf_bool_true,
        "emf_event_get_event_handle(): The event must already exist."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .event_get_event_handle_fn
        .unwrap()(event_name)
}

/// Checks for the existence of a handle.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_event_handle_exists(event_handle: emf_event_handle_t) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .event_handle_exists_fn
        .unwrap()(event_handle)
}

/// Checks for the existence of a name.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_name_exists};
/// # let event_name: *const emf_event_name_t = std::ptr::null();
/// assert_ne!(
///     event_name,
///     std::ptr::null()
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_name_exists(event_name: *const emf_event_name_t) -> emf_bool_t {
    emf_debug_assert_ne!(
        event_name,
        std::ptr::null(),
        "emf_event_name_exists(): `event_name` may not be `NULL`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .event_name_exists_fn
        .unwrap()(event_name)
}

/// Subscribes to an event.
///
/// The `event_handler` is added to the end of the call list.
///
/// # Event input
///
/// Not every `event_handler` is compatible with an event.
/// The creator of an event must define how the input will be interpreted.
///
/// Incorrect usage of an event may lead errors that are difficult to find.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_handle_exists};
/// # let event_handle = Default::default();
/// # let event_handler: emf_event_handler_fn_t = None;
/// assert_eq!(
///     unsafe { emf_event_handle_exists(event_handle) },
///     emf_bool_true
/// );
/// assert_ne!(
///     event_handler,
///     None
/// );
/// ```
///
/// Subscribing to the same event multiple times with the same
/// `event_handler` will lead to undefined behaviour.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_subscribe_handler(
    event_handle: emf_event_handle_t,
    event_handler: emf_event_handler_fn_t,
) {
    emf_debug_assert_eq!(
        emf_event_handle_exists(event_handle),
        emf_bool_true,
        "emf_event_subscribe_handler(): The event must already exist."
    );
    emf_debug_assert_ne!(
        event_handler,
        None,
        "emf_event_subscribe_handler(): `event_handler` may not be `None`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .event_subscribe_handler_fn
        .unwrap()(event_handle, event_handler)
}

/// Unsubscribes from an event.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_handle_exists};
/// # let event_handle = Default::default();
/// # let event_handler: emf_event_handler_fn_t = None;
/// assert_eq!(
///     unsafe { emf_event_handle_exists(event_handle) },
///     emf_bool_true
/// );
/// assert_ne!(
///     event_handler,
///     None
/// );
/// ```
///
/// Unsubscribing if not already subscribed, will lead to undefined behaviour.
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_unsubscribe_handler(
    event_handle: emf_event_handle_t,
    event_handler: emf_event_handler_fn_t,
) {
    emf_debug_assert_eq!(
        emf_event_handle_exists(event_handle),
        emf_bool_true,
        "emf_event_subscribe_handler(): The event must already exist."
    );
    emf_debug_assert_ne!(
        event_handler,
        None,
        "emf_event_subscribe_handler(): `event_handler` may not be `None`."
    );

    super::S_CORE_INTERFACE
        .unwrap()
        .event_unsubscribe_handler_fn
        .unwrap()(event_handle, event_handler)
}

/// Fires an event.
///
/// # Event input
///
/// The parameter `event_data` is passed to every handler in the event.
/// The creator of the event should define the structure, mutability and valid operations of the input.
///
/// See [`emf_event_subscribe_handler()`](./fn.emf_event_subscribe_handler.html) for more information.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::event::{emf_event_handle_exists};
/// # let event_handle = Default::default();
/// assert_eq!(
///     unsafe { emf_event_handle_exists(event_handle) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_event_signal(
    event_handle: emf_event_handle_t,
    event_data: emf_event_data_t,
) {
    emf_debug_assert_eq!(
        emf_event_handle_exists(event_handle),
        emf_bool_true,
        "emf_event_signal(): The event must already exist."
    );

    super::S_CORE_INTERFACE.unwrap().event_signal_fn.unwrap()(event_handle, event_data)
}
