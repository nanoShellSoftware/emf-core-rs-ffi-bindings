use crate::*;

#[no_mangle]
pub unsafe extern "C" fn emf_library_register_loader(
    loader_interface: *const emf_library_loader_interface_t,
    library_type: *const emf_library_type_t,
) -> emf_library_loader_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_register_loader_fn
        .unwrap()(loader_interface, library_type)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unregister_loader(loader_handle: emf_library_loader_handle_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_unregister_loader_fn
        .unwrap()(loader_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_get_num_loaders() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_get_num_loaders_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_get_library_types(
    buffer: *mut emf_library_type_span_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_get_library_types_fn
        .unwrap()(buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_get_loader_handle(
    library_type: *const emf_library_type_t,
) -> emf_library_loader_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_get_loader_handle_fn
        .unwrap()(library_type)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_type_exists(
    library_type: *const emf_library_type_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_type_exists_fn
        .unwrap()(library_type)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_library_exists(
    library_handle: emf_library_handle_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_library_exists_fn
        .unwrap()(library_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unsafe_create_library_handle() -> emf_library_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_unsafe_create_library_handle_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unsafe_remove_library_handle(
    library_handle: emf_library_handle_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_unsafe_remove_library_handle_fn
        .unwrap()(library_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unsafe_link_library(
    library_handle: emf_library_handle_t,
    loader_handle: emf_library_loader_handle_t,
    loader_library_handle: emf_library_loader_library_handle_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_unsafe_link_library_fn
        .unwrap()(library_handle, loader_handle, loader_library_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unsafe_get_loader_library_handle(
    library_handle: emf_library_handle_t,
) -> emf_library_loader_library_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_unsafe_get_loader_library_handle_fn
        .unwrap()(library_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unsafe_get_loader(
    loader_handle: emf_library_loader_handle_t,
) -> emf_library_loader_interface_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_unsafe_get_loader_fn
        .unwrap()(loader_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_load(
    loader_handle: emf_library_loader_handle_t,
    library_path: *const emf_path_t,
) -> emf_library_handle_t {
    super::S_CORE_INTERFACE.unwrap().library_load_fn.unwrap()(loader_handle, library_path)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_unload(library_handle: emf_library_handle_t) {
    super::S_CORE_INTERFACE.unwrap().library_unload_fn.unwrap()(library_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_get_symbol(
    library_handle: emf_library_handle_t,
    symbol_name: *const ::std::os::raw::c_char,
    symbol: *mut emf_symbol_t,
) -> emf_error_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_get_symbol_fn
        .unwrap()(library_handle, symbol_name, symbol)
}

#[no_mangle]
pub unsafe extern "C" fn emf_library_get_function_symbol(
    library_handle: emf_library_handle_t,
    symbol_name: *const ::std::os::raw::c_char,
    symbol: *mut emf_fn_symbol_t,
) -> emf_error_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .library_get_function_symbol_fn
        .unwrap()(library_handle, symbol_name, symbol)
}
