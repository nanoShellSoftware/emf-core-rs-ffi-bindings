//! Bindings for the `core` system.
//!
//! The `core` system provides an interface
//! to the core functionalities of the `emf-core` interface.
use crate::*;

/// Locks the systems.
///
/// The calling thread is stalled until the lock can be acquired.
/// Only one thread can hold the lock at a time.
///
/// # Deadlock
///
/// Calling this function while the calling thread holds a lock may result in a deadlock.
#[no_mangle]
pub unsafe extern "C" fn emf_lock() {
    super::S_CORE_INTERFACE.unwrap().lock_fn.unwrap()()
}

/// Tries to lock the systems.
///
/// The function fails if another thread already holds the lock.
#[no_mangle]
pub unsafe extern "C" fn emf_try_lock() -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().try_lock_fn.unwrap()()
}

/// Unlocks the systems.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_unlock() {
    super::S_CORE_INTERFACE.unwrap().unlock_fn.unwrap()()
}

/// Sends a termination signal.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_shutdown() {
    super::S_CORE_INTERFACE.unwrap().shutdown_fn.unwrap()()
}

/// Panics.
///
/// Execution of the program is stopped abruptly.
///
/// # Tests
/// If called during a test (or doctest),
/// the function calls `panic!` without the terminating `\0` character.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
#[cfg(not(test))]
pub unsafe extern "C" fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
    super::S_CORE_INTERFACE.unwrap().panic_fn.unwrap()(error);
    unreachable!();
}

#[no_mangle]
#[cfg(test)]
pub unsafe extern "C" fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
    if !error.is_null() {
        let error_len = libc::strlen(error);
        let error_slice =
            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
        let error_msg = std::str::from_utf8(error_slice).unwrap();
        panic!(error_msg);
    } else {
        panic!();
    }
}

/// Checks if a function is implemented.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_has_function(fn_id: emf_fn_ptr_id_t) -> emf_bool_t {
    super::S_CORE_INTERFACE.unwrap().has_function_fn.unwrap()(fn_id)
}

/// Retrieves the function pointer to the supplied function.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).<br>
/// Furthermore, the caller must ensure that the following preconditions hold:
/// ```no_run
/// # use emf_core_rs_ffi_bindings::*;
/// # use emf_core_rs_ffi_bindings::bindings::core::{emf_has_function};
/// # let fn_id = emf_fn_ptr_id_lock;
/// assert_eq!(
///     unsafe { emf_has_function(fn_id) },
///     emf_bool_true
/// );
/// ```
///
/// # Note
///
/// The preconditions may not be checked at runtime.
/// See [`emf_debug_assert!`](../../macro.emf_debug_assert.html) for more information.
#[no_mangle]
pub unsafe extern "C" fn emf_get_function(fn_id: emf_fn_ptr_id_t) -> emf_fn_t {
    emf_debug_assert_eq!(
        emf_has_function(fn_id),
        emf_bool_true,
        "emf_get_function(): The function {} does not exist.",
        fn_id
    );

    super::S_CORE_INTERFACE.unwrap().get_function_fn.unwrap()(fn_id)
}

/// Fetches the current synchronisation handler.
///
/// The result of this call will never be `std::ptr::null()`.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
#[no_mangle]
pub unsafe extern "C" fn emf_get_sync_handler() -> *const emf_sync_handler_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .get_sync_handler_fn
        .unwrap()()
}

/// Sets a new synchronisation handler.
///
/// The default synchronisation handler is set, if `sync_handler` is `std::ptr::null()`.
///
/// # Uses
///
/// This function can be used by modules, that want to provide a more complex
/// synchronisation mechanism than the one presented by the default handler.
///
/// # Swapping
///
/// The swapping occurs in three steps:
/// 1. The new synchronisation handler is locked.
/// 2. The new synchronisation handler is set as the main handler.
/// 3. The old synchronisation handler is unlocked.
///
/// # Undefined Behaviour
///
/// The callee expects that the caller holds a lock (See [`emf_lock()`](../core/fn.emf_lock.html)).
///
/// # Warning
///
/// Changing the synchronisation handler may break some modules,
/// if they depend on a specific synchronisation handler.
#[no_mangle]
pub unsafe extern "C" fn emf_set_sync_handler(sync_handler: *const emf_sync_handler_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .set_sync_handler_fn
        .unwrap()(sync_handler)
}
