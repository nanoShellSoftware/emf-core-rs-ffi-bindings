use crate::*;

#[no_mangle]
pub unsafe extern "C" fn emf_module_register_loader(
    loader_interface: *const emf_module_loader_interface_t,
    module_type: *const emf_module_type_t,
) -> emf_module_loader_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_register_loader_fn
        .unwrap()(loader_interface, module_type)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unregister_loader(loader_handle: emf_module_loader_handle_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_unregister_loader_fn
        .unwrap()(loader_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_num_loaders() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_num_loaders_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_module_types(buffer: *mut emf_module_type_span_t) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_module_types_fn
        .unwrap()(buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_num_modules() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_num_modules_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_modules(buffer: *mut emf_module_info_span_t) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_modules_fn
        .unwrap()(buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_num_public_interfaces() -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_num_public_interfaces_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_public_interfaces(
    buffer: *mut emf_interface_descriptor_span_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_public_interfaces_fn
        .unwrap()(buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_loader_handle(
    module_type: *const emf_module_type_t,
) -> emf_module_loader_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_loader_handle_fn
        .unwrap()(module_type)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_type_exists(
    module_type: *const emf_module_type_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_type_exists_fn
        .unwrap()(module_type)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_module_exists(
    module_handle: emf_module_handle_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_module_exists_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_interface_handle(
    interface: *const emf_interface_info_t,
) -> emf_module_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_interface_handle_fn
        .unwrap()(interface)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_interface_exists(
    interface: *const emf_interface_info_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_interface_exists_fn
        .unwrap()(interface)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unsafe_create_module_handle() -> emf_module_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_unsafe_create_module_handle_fn
        .unwrap()()
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unsafe_remove_module_handle(
    module_handle: emf_module_handle_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_unsafe_remove_module_handle_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unsafe_link_module(
    module_handle: emf_module_handle_t,
    loader_handle: emf_module_loader_handle_t,
    loader_module_handle: emf_module_loader_module_handle_t,
    public_interfaces: *const emf_interface_info_span_t,
) {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_unsafe_link_module_fn
        .unwrap()(
        module_handle,
        loader_handle,
        loader_module_handle,
        public_interfaces,
    )
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unsafe_get_loader_module_handle(
    module_handle: emf_module_handle_t,
) -> emf_module_loader_module_handle_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_unsafe_get_loader_module_handle_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unsafe_get_loader(
    loader_handle: emf_module_loader_handle_t,
) -> emf_module_loader_interface_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_unsafe_get_loader_fn
        .unwrap()(loader_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_load(
    loader_handle: emf_module_loader_handle_t,
    public_interfaces: *const emf_interface_info_span_t,
    module_path: *const emf_path_t,
) -> emf_module_handle_t {
    super::S_CORE_INTERFACE.unwrap().module_load_fn.unwrap()(
        loader_handle,
        public_interfaces,
        module_path,
    )
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_unload(module_handle: emf_module_handle_t) {
    super::S_CORE_INTERFACE.unwrap().module_unload_fn.unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_initialize(module_handle: emf_module_handle_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_initialize_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_terminate(module_handle: emf_module_handle_t) {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_terminate_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_module_info(
    module_handle: emf_module_handle_t,
) -> *const emf_module_info_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_module_info_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_exported_interfaces(
    module_handle: emf_module_handle_t,
) -> *const emf_interface_descriptor_span_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_exported_interfaces_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_imported_interfaces(
    module_handle: emf_module_handle_t,
) -> *const emf_interface_descriptor_span_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_imported_interfaces_fn
        .unwrap()(module_handle)
}

#[no_mangle]
pub unsafe extern "C" fn emf_module_get_interface(
    module_handle: emf_module_handle_t,
    interface_info: *const emf_interface_info_t,
    interface: *mut emf_interface_t,
) -> emf_error_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .module_get_interface_fn
        .unwrap()(module_handle, interface_info, interface)
}
