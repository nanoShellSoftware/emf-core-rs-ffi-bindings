use crate::*;

#[no_mangle]
pub unsafe extern "C" fn emf_version_construct_short(
    major: i32,
    minor: i32,
    patch: i32,
) -> emf_version_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_construct_short_fn
        .unwrap()(major, minor, patch)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_construct_long(
    major: i32,
    minor: i32,
    patch: i32,
    release_type: emf_version_release_t,
    release_number: i8,
) -> emf_version_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_construct_long_fn
        .unwrap()(major, minor, patch, release_type, release_number)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_construct_full(
    major: i32,
    minor: i32,
    patch: i32,
    release_type: emf_version_release_t,
    release_number: i8,
    build: i64,
) -> emf_version_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_construct_full_fn
        .unwrap()(major, minor, patch, release_type, release_number, build)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_construct_from_string(
    version_string: *const ::std::os::raw::c_char,
) -> emf_version_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_construct_from_string_fn
        .unwrap()(version_string)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_representation_is_valid(
    version_string: *const ::std::os::raw::c_char,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_representation_is_valid_fn
        .unwrap()(version_string)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_get_short_representation(
    version: *const emf_version_t,
    buffer: *mut emf_version_representation_buffer_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_get_short_representation_fn
        .unwrap()(version, buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_get_short_representation_size(
    version: *const emf_version_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_get_short_representation_size_fn
        .unwrap()(version)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_get_long_representation(
    version: *const emf_version_t,
    buffer: *mut emf_version_representation_buffer_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_get_long_representation_fn
        .unwrap()(version, buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_get_long_representation_size(
    version: *const emf_version_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_get_long_representation_size_fn
        .unwrap()(version)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_get_full_representation(
    version: *const emf_version_t,
    buffer: *mut emf_version_representation_buffer_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_get_full_representation_fn
        .unwrap()(version, buffer)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_get_full_representation_size(
    version: *const emf_version_t,
) -> usize {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_get_full_representation_size_fn
        .unwrap()(version)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_compare(
    lhs: *const emf_version_t,
    rhs: *const emf_version_t,
) -> i32 {
    super::S_CORE_INTERFACE.unwrap().version_compare_fn.unwrap()(lhs, rhs)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_compare_weak(
    lhs: *const emf_version_t,
    rhs: *const emf_version_t,
) -> i32 {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_compare_weak_fn
        .unwrap()(lhs, rhs)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_compare_strong(
    lhs: *const emf_version_t,
    rhs: *const emf_version_t,
) -> i32 {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_compare_strong_fn
        .unwrap()(lhs, rhs)
}

#[no_mangle]
pub unsafe extern "C" fn emf_version_is_compatible(
    lhs: *const emf_version_t,
    rhs: *const emf_version_t,
) -> emf_bool_t {
    super::S_CORE_INTERFACE
        .unwrap()
        .version_is_compatible_fn
        .unwrap()(lhs, rhs)
}
